//
//  UIViewCategory.m
//  Pong
//
//  Created by Johan H on 2015-02-09.
//  Copyright (c) 2015 Johan H. All rights reserved.
//

#import "UIViewCategory.h"


@implementation UIView (UIViewCategory)

-(void)adjustWithBorder:(float)border {
    
    if (self.superview) {
        CGRect newFrame = CGRectMake(0.0f, 0.0f, self.superview.frame.size.width - border * 0.0f, self.superview.frame.size.height - border * 0.0f);
        self.frame = newFrame;
        self.center = self.superview.center;
    }
    
}
const float RAD_TO_DEG = 57.296f;
const float DEG_TO_RAD = 1.0f / RAD_TO_DEG;


-(void)rotateWithdDegrees:(float)degrees{
    CGAffineTransform oldTransform = self.transform;
    CGAffineTransform newTransform = CGAffineTransformRotate(oldTransform, degrees * DEG_TO_RAD);
    self.transform = newTransform;
}

@end
