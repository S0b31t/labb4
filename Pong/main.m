//
//  main.m
//  Pong
//
//  Created by Johan H on 2015-02-09.
//  Copyright (c) 2015 Johan H. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
