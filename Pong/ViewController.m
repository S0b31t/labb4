//
//  ViewController.m
//  Pong
//
//  Created by Johan H on 2015-02-09.
//  Copyright (c) 2015 Johan H. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *fire1;

@end

@implementation ViewController

-(void)Collision{
    
    if (CGRectIntersectsRect(fireBox.frame, flamingBar.frame)) {
        for (int i =0; i <=1; i++){
            Y++;
            
        }
        Y= 0-Y;
        NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/explosion.wav",[[NSBundle mainBundle] resourcePath]]];
                      
                      NSError *errror;
                      sound = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&errror];
                      sound.numberOfLoops = 0;
                      
                      [sound play];
    }
    
    if(CGRectIntersectsRect(fireBox.frame, flamingBarComp.frame)){
        for (int i =0; i <=1; i++){
            Y++;
        }
        NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/explosion.wav",[[NSBundle mainBundle] resourcePath]]];
        
        NSError *error;
        sound = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
        sound.numberOfLoops = 0;
        
        [sound play];
    }
    
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    
    UITouch *Drag = [[event allTouches] anyObject];
    flamingBar.center = [Drag locationInView:self.view];
    
    if (flamingBar.center.y > 457.5) {
        flamingBar.center = CGPointMake(flamingBar.center.x, 457.5);
    }
    
    if (flamingBar.center.y < 457.5) {
        flamingBar.center = CGPointMake(flamingBar.center.x, 457.5);
    }
    
    if (flamingBar.center.x < 60) {
        flamingBar.center = CGPointMake(60, flamingBar.center.y);
    }
    
    if (flamingBar.center.x > 260) {
        flamingBar.center = CGPointMake(260, flamingBar.center.y);
    }
}

-(void)CompAnimation{
    
    
    if (flamingBarComp.center.x > fireBox.center.x) {
        flamingBarComp.center = CGPointMake(flamingBarComp.center.x - 1, flamingBarComp.center.y);
    }
    
    if(flamingBarComp.center.x < fireBox.center.x){
        flamingBarComp.center = CGPointMake(flamingBarComp.center.x + 1, flamingBarComp.center.y);
    }
    
    if (flamingBarComp.center.x < 60) {
        flamingBarComp.center = CGPointMake(60, flamingBarComp.center.y);
    }
    
    if (flamingBarComp.center.x > 260) {
        flamingBarComp.center = CGPointMake(260, flamingBarComp.center.y);
    }
    
    
}

-(IBAction)StartButton:(id)sender{
    
    AnimatedBackground.hidden = YES;
    StartButton.hidden = YES;
    WinLose.hidden = YES;
    
    ScorePlayerNr = 0;
    ScoreCompNr = 0;
    ScorePlayerLabel.text = @"0";
    ScoreCompLabel.text = @"0";
    
    Y = arc4random() % 4;
    Y = Y - 2;
    
    X = arc4random() % 4;
    X = X - 2;
    
    if (Y==0) {
        Y = 1;
    }
    
    if (X==0) {
        X = 1;
    }
    
    timer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(BallAnimation) userInfo:nil repeats:YES];

}

-(void)Continue{
    
    Y = arc4random() % 8;
    Y = Y - 4;
    
    X = arc4random() % 8;
    X = X - 4;
    
    if (Y==0) {
        Y = 1;
    }
    
    if (X==0) {
        X = 1;
    }
}

-(void)BallAnimation{
    
    [self CompAnimation];
    [self Collision];
    
    fireBox.center = CGPointMake(fireBox.center.x + X, fireBox.center.y + Y);
    
    if (fireBox.center.x < 10 ) {
        X = 0 - X;
    }
    if (fireBox.center.x > 310) {
        X = 0 - X;
    }
    
    if (fireBox.center.y < 0) {
        NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/glass.wav",[[NSBundle mainBundle] resourcePath]]];
        
        NSError *errror;
        sound = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&errror];
        sound.numberOfLoops = 0;
        
        [sound play];
        
        ScorePlayerNr = ScorePlayerNr + 1;
        ScorePlayerLabel.text = [NSString stringWithFormat:@"%i", ScorePlayerNr];
        
        [self Continue];
        
        fireBox.center = CGPointMake(160, 233);
        flamingBarComp.center = CGPointMake(160, 22.5);
        
        if (ScorePlayerNr == 10) {
            NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/boom4.wav",[[NSBundle mainBundle] resourcePath]]];
            
            NSError *errror;
            sound = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&errror];
            sound.numberOfLoops = 0;
            
            [sound play];
            
            AnimatedBackground.hidden = NO;
            StartButton.hidden = YES;
            WinLose.hidden = NO;
            WinLose.text = [NSString stringWithFormat:@"YOU WIN!"];
            [timer invalidate];
            StartButton.hidden = NO;
        }
    }
    
        if (fireBox.center.y > 460) {
            NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Punch.wav",[[NSBundle mainBundle] resourcePath]]];
            
            NSError *errror;
            sound = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&errror];
            sound.numberOfLoops = 0;
            
            [sound play];
            
            ScoreCompNr = ScoreCompNr + 1;
            ScoreCompLabel.text = [NSString stringWithFormat:@"%i", ScoreCompNr];
        
        [self Continue];
        
        fireBox.center = CGPointMake(160, 233);
        flamingBarComp.center = CGPointMake(160, 22.5);
            
        if (ScoreCompNr == 10) {
            NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Creepy.wav",[[NSBundle mainBundle] resourcePath]]];
            
            NSError *errror;
            sound = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&errror];
            sound.numberOfLoops = 0;
        
            [sound play];
            
            AnimatedBackground.hidden = NO;
            StartButton.hidden = YES;
            WinLose.hidden = NO;
            WinLose.text = [NSString stringWithFormat:@"YOU LOSE!"];
            [timer invalidate];
            StartButton.hidden = NO;
        }
    }
}

- (void)viewDidLoad {
    WinLose.hidden = YES;
    
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Frying.wav",[[NSBundle mainBundle] resourcePath]]];
    
    NSError *errror;
    sound = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&errror];
    sound.numberOfLoops = 10;
    
    [sound play];
    
        AnimatedBackground.animationImages = [NSArray arrayWithObjects:
                                              [UIImage imageNamed:@"background1.png"],
                                              [UIImage imageNamed:@"background2.png"],
                                              [UIImage imageNamed:@"background3.png"],
                                              [UIImage imageNamed:@"background4.png"], nil];
        
        
        [AnimatedBackground setAnimationRepeatCount:0];
        AnimatedBackground.animationDuration = 1;
        [AnimatedBackground startAnimating];
    
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews {
    // possible of use later on

}

@end
