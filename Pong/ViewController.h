//
//  ViewController.h
//  Pong
//
//  Created by Johan H on 2015-02-09.
//  Copyright (c) 2015 Johan H. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

int Y;
int X;
int ScorePlayerNr;
int ScoreCompNr;

@interface ViewController : UIViewController

{    
    IBOutlet UIImageView *fireBox;
    IBOutlet UIButton *StartButton;
    IBOutlet UIImageView *flamingBar;
    IBOutlet UIImageView *flamingBarComp;
    IBOutlet UILabel *ScorePlayerLabel;
    IBOutlet UILabel *ScoreCompLabel;
    IBOutlet UILabel *WinLose;
    IBOutlet UIImageView *AnimatedBackground;
    
    NSTimer *timer;
    AVAudioPlayer *sound;
}

-(IBAction)StartButton:(id)sender;
-(void)BallAnimation;
-(void)CompAnimation;
-(void)Collision;

@end

