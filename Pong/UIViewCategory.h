//
//  UIViewCategory.h
//  Pong
//
//  Created by Johan H on 2015-02-09.
//  Copyright (c) 2015 Johan H. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (UIViewCategory)

-(void)adjustWithBorder:(float)border;
-(void)rotateWithdDegrees:(float)degrees;


@end
